sudo docker network create monitornet
sudo docker run -d --name elasticsearch --net monitornet -p 9200:9200 -p 9300:9300 \
	-e "discovery.type=single-node" $ELASTIC_SEARCH_IMAGE
sudo docker run -d --name kibana --net monitornet -p 5601:5601 $KIBANA_IMAGE
