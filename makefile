# Tasks for maintaining the course's reference implemenetations.

.PHONY: commitall

# Commit all changes to all of the course's repos.
commit:
	{ \
	git add . ; \
	git commit -am misc; \
	git push; \
	}
	# Module 3
	{ \
	pushd ../devops-1; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	# Module 6
	{ \
	pushd ../devops-2-bdd; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	# Module 7
	{ \
	pushd ../devops-3b-bdd-modules; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	# Module 8
	{ \
	pushd ../devops-4-bdd-modules-docker; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	# Module 11
	{ \
	pushd ../devops-5-component; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	# Module 14
	{ \
	pushd ../devops-6-component; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	# Modules 15 & 16
	{ \
	pushd ../devops-7-hello; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	{ \
	pushd ../devops-7-wegood; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
	{ \
	pushd ../devops-7-product; \
	git add . ; \
	git commit -am misc; \
	git push; \
	popd; \
	}
